(setq user-full-name "Currell Berry") 
(require 'package)
(push '("marmalade" . "http://marmalade-repo.org/packages/")
       package-archives )
(push '("melpa" . "http://melpa.milkbox.net/packages/")
       package-archives)
(package-initialize)

;; replace Arial with Liberation Sans on linux

(require 'evil)
(evil-mode nil)

(global-set-key (kbd "C-x o") 'ace-window)

;;When running in Windows, we want to use an alternate shell so we
;;can be more unixy.
;(setq shell-file-name "C:/MinGW/msys/1.0/bin/bash")
;(setq explicit-shell-file-name shell-file-name)
;CB TODO fix path below...

;(setenv "PATH"
;    (concat ":/usr/local/bin:/mingw/bin:/bin;"
;	(getenv "PATH")))

;(setq inferior-lisp-program "C:/Users/vancan1ty/software/ccl-1.10-windowsx86/ccl/wx86cl64.exe")

;(setenv "PATH"
;    (concat ":/usr/local/bin:/mingw/bin:/bin:"
;            (replace-regexp-in-string "\\\\" "/"
;                (replace-regexp-in-string "\\([A-Za-z]\\):" "/\\1"
;					  (getenv "PATH"))
;            t t)
;       ))

;(defun cygwin-shell ()
;  "Run cygwin bash in shell mode."
;  (interactive)
;  (let ((shell-file-name "C:/cygwin64/bin/bash")
;	(explicit-shell-file-name "C:/cygwin64/bin/bash"))
;    (call-interactively 'shell)))

;(require 'server)
;(unless (server-running-p)
;  (server-start)) 

;; I don't really want to see the startup screen every time...
(setq inhibit-startup-message t)

(defface monoface '((t :family "DejaVu Sans Mono" :height 110)) "monospaced face I use in various places")

(custom-set-variables
 ;; custom-set-variables was added by Custom.
 ;; If you edit it by hand, you could mess it up, so be careful.
 ;; Your init file should contain only one such instance.
 ;; If there is more than one, they won't work right.
 '(ansi-color-names-vector
   ["#073642" "#dc322f" "#859900" "#b58900" "#268bd2" "#d33682" "#2aa198" "#657b83"])
 '(column-number-mode t)
 '(compilation-message-face (quote default))
 '(cua-global-mark-cursor-color "#2aa198")
 '(cua-normal-cursor-color "#839496")
 '(cua-overwrite-cursor-color "#b58900")
 '(cua-read-only-cursor-color "#859900")
 '(custom-safe-themes
   (quote
    ("d677ef584c6dfc0697901a44b885cc18e206f05114c8a3b7fde674fce6180879" "8aebf25556399b58091e533e455dd50a6a9cba958cc4ebb0aab175863c25b9a4" default)))
 '(fci-rule-color "#073642")
 '(highlight-changes-colors (quote ("#d33682" "#6c71c4")))
 '(highlight-symbol-colors
   (--map
    (solarized-color-blend it "#002b36" 0.25)
    (quote
     ("#b58900" "#2aa198" "#dc322f" "#6c71c4" "#859900" "#cb4b16" "#268bd2"))))
 '(highlight-symbol-foreground-color "#93a1a1")
 '(highlight-tail-colors
   (quote
    (("#073642" . 0)
     ("#546E00" . 20)
     ("#00736F" . 30)
     ("#00629D" . 50)
     ("#7B6000" . 60)
     ("#8B2C02" . 70)
     ("#93115C" . 85)
     ("#073642" . 100))))
 '(hl-bg-colors
   (quote
    ("#7B6000" "#8B2C02" "#990A1B" "#93115C" "#3F4D91" "#00629D" "#00736F" "#546E00")))
 '(hl-fg-colors
   (quote
    ("#002b36" "#002b36" "#002b36" "#002b36" "#002b36" "#002b36" "#002b36" "#002b36")))
 '(magit-diff-use-overlays nil)
 '(nrepl-message-colors
   (quote
    ("#dc322f" "#cb4b16" "#b58900" "#546E00" "#B4C342" "#00629D" "#2aa198" "#d33682" "#6c71c4")))
 '(pos-tip-background-color "#073642")
 '(pos-tip-foreground-color "#93a1a1")
 '(safe-local-variable-values (quote ((c-noise-macro-names "UNINIT"))))
 '(smartrep-mode-line-active-bg (solarized-color-blend "#859900" "#073642" 0.2))
 '(term-default-bg-color "#002b36")
 '(term-default-fg-color "#839496")
 '(vc-annotate-background nil)
 '(vc-annotate-background-mode nil)
 '(vc-annotate-color-map
   (quote
    ((20 . "#dc322f")
     (40 . "#c85d17")
     (60 . "#be730b")
     (80 . "#b58900")
     (100 . "#a58e00")
     (120 . "#9d9100")
     (140 . "#959300")
     (160 . "#8d9600")
     (180 . "#859900")
     (200 . "#669b32")
     (220 . "#579d4c")
     (240 . "#489e65")
     (260 . "#399f7e")
     (280 . "#2aa198")
     (300 . "#2898af")
     (320 . "#2793ba")
     (340 . "#268fc6")
     (360 . "#268bd2"))))
 '(vc-annotate-very-old-color nil)
 '(weechat-color-list
   (quote
    (unspecified "#002b36" "#073642" "#990A1B" "#dc322f" "#546E00" "#859900" "#7B6000" "#b58900" "#00629D" "#268bd2" "#93115C" "#d33682" "#00736F" "#2aa198" "#839496" "#657b83")))
 '(xterm-color-names
   ["#073642" "#dc322f" "#859900" "#b58900" "#268bd2" "#d33682" "#2aa198" "#eee8d5"])
 '(xterm-color-names-bright
   ["#002b36" "#cb4b16" "#586e75" "#657b83" "#839496" "#6c71c4" "#93a1a1" "#fdf6e3"]))
(custom-set-faces
 ;; custom-set-faces was added by Custom.
 ;; If you edit it by hand, you could mess it up, so be careful.
 ;; Your init file should contain only one such instance.
 ;; If there is more than one, they won't work right.
 '(default ((t (:family "Liberation Sans" :foundry "outline" :slant normal :weight normal :height 120 :width normal)))))

(defun my-adjoin-to-list-or-symbol (element list-or-symbol)
  (let ((list (if (not (listp list-or-symbol))
                  (list list-or-symbol)
                list-or-symbol)))
    (require 'cl-lib)
    (cl-adjoin element list)))

 ;; Use variable width font faces in current buffer
 (defun my-buffer-face-mode-variable ()
   "Set font to a variable width (proportional) fonts in current buffer"
   (interactive)
   (setq buffer-face-mode-face '(:family "Liberation Sans" :foundry "outline" :slant normal :weight normal :height 120 :width normal))
   (buffer-face-mode))

 ;; Use monospaced font faces in current buffer
 (defun my-buffer-face-mode-fixed ()
   "Sets a fixed width (monospace) font in current buffer"
   (interactive)
   (setq buffer-face-mode-face '(:family "DejaVu Sans Mono" :height 110))
   (face-remap-add-relative 'header-line 'monoface)
   (buffer-face-mode))

(add-hook 'java-mode-hook 'my-buffer-face-mode-fixed)
(add-hook 'calendar-mode-hook 'my-buffer-face-mode-fixed)
(add-hook 'c-mode-hook 'my-buffer-face-mode-fixed)
(add-hook 'slime-popup-buffer-mode-hook 'my-buffer-face-mode-fixed)

 (define-minor-mode evil-visual-line-movement-mode 
    "Minor mode which provides visual-line movement, primarily intended 
     for org mode documents. "
    :init-value nil)

 (define-minor-mode  monospace-mode
    "Minor mode which provides visual-line movement, primarily intended 
     for org mode documents. "
    :init-value nil)

(defun xah-set-font-to-monospace ()
  "Change font in current window to a monospaced font."
  (interactive)
  (set-frame-font "DejaVu Sans Mono" t)
  (text-scale-set 0.5)
  )

(load-theme 'leuven t)

(add-hook 'org-mode-hook 
          (lambda ()
            ;;first, set code, tables, blocks, etc... to use monospace
            (mapc
             (lambda (face)
               (set-face-attribute
                face nil
                :inherit
                (my-adjoin-to-list-or-symbol
                 'fixed-pitch
                 (face-attribute face :inherit))))
             (list 'org-code 'org-table 'org-block 'org-block-background))
            ;;(define-key my-org-buffer-local-mode-map (kbd "<f10>") 'some-custom-defun-specific-to-this-buffer)
            ;;(evil-define-key 'normal evil-visual-line-movement-mode-map "j" 'evil-next-visual-line)
            ))

(evil-define-motion my-0-key-alteration ()
  (if (eql major-mode 'org-mode)
      (evil-beginning-of-visual-line)
    (evil-beginning-of-line)
  ))
(evil-define-key 'normal org-mode-map  "k" 'evil-previous-visual-line)
(evil-define-key 'normal org-mode-map "j" 'evil-next-visual-line)
(evil-define-key 'normal org-mode-map "gj" 'evil-next-line)
(evil-define-key 'normal org-mode-map "gk" 'evil-previous-line)
(evil-define-key 'normal org-mode-map "g0" 'evil-beginning-of-line)
(evil-redirect-digit-argument evil-motion-state-map "0" 'my-0-key-alteration) 
(evil-define-key 'normal org-mode-map "g$" 'evil-end-of-line)
(evil-define-key 'normal org-mode-map "$" 'evil-end-of-visual-line)

(set-window-margins nil 0 0)

(defun hide-fringes ()
  (set-fringe-mode 0))

(defun show-fringes ()
  (set-fringe-mode 1))

;; (add-hook 'org-mode-hook
;;           (lambda ()
;;            (set-window-margins (current-buffer) nil t)) 1 1)
;; (add-hook 'window-configuration-change-hook
;;           (lambda ()
;;            (set-window-margins (car (get-buffer-window-list (current-buffer) nil t)) 1 1)))

(set-face-attribute 'fringe nil :background "#FFFFFF" :foreground "#000000")

(tool-bar-mode -1)

(setq scroll-step 1) ;; keyboard scroll one line at a time

(setq-default indent-tabs-mode nil)

(add-hook 'lisp-mode-hook
	   (lambda ()
	     (set (make-local-variable 'lisp-indent-function)
		  'common-lisp-indent-function)))

(org-babel-do-load-languages
 'org-babel-load-languages
 '((dot . t)
   (emacs-lisp . t))) ; this line activates dot

;(setq python-shell-interpreter "C:\\Anaconda\\python.exe"
;      python-shell-interpreter-args "-i C:\\Anaconda\\Scripts\\ipython-script.py console --matplotlib")

(setq ido-enable-flex-matching t)
(setq ido-everywhere t)
(ido-mode 1)

(setq org-confirm-babel-evaluate nil)

; use utf-8! details:
; http://www.masteringemacs.org/articles/2012/08/09/working-coding-systems-unicode-emacs/
(prefer-coding-system 'utf-8)
(set-default-coding-systems 'utf-8)
(set-terminal-coding-system 'utf-8)
(set-keyboard-coding-system 'utf-8)
;; Treat clipboard input as UTF-8 string first; compound text next, etc.
(setq x-select-request-type '(UTF8_STRING COMPOUND_TEXT TEXT STRING))

;;set up preferred frame sizing.
;; (when window-system          ; start speedbar if we're using a window system
;;    (set-frame-size (selected-frame) 1100 690 t)
;;    (set-frame-position (selected-frame) 215 0)
;;    (speedbar t)
;;    ;(select-frame-by-name "Speedbar 1.0")
;;    ;(set-frame-size (selected-frame) 170 710 t)
;;    ;(set-frame-position (selected-frame) 0 0)
;;    ;(other-frame 1)
;;    )

(put 'narrow-to-region 'disabled nil)
(put 'downcase-region 'disabled nil)

(require 'evil-numbers)
(global-set-key (kbd "C-c +") 'evil-numbers/inc-at-pt)
(global-set-key (kbd "C-c -") 'evil-numbers/dec-at-pt)
(define-key evil-normal-state-map (kbd "C-c +") 'evil-numbers/inc-at-pt)
(define-key evil-normal-state-map (kbd "C-c -") 'evil-numbers/dec-at-pt)

;(setenv "GS_LIB" "C:/Program Files/gs/gs9.09/lib;")
;(setq ps-lpr-command "C:/Program Files/gs/gs9.09/bin/gswin64c.exe")
;(setq ps-lpr-switches '("-q" "-dNOPAUSE" "-dBATCH" "-sDEVICE=mswinpr2"))
;(setq ps-printer-name t)
;
;(setq ps-paper-type 'a4
;      ps-font-size 7.0
;      ps-print-header nil
;      ps-landscape-mode t
;      ps-number-of-columns 3
;      ps-line-number nil 
;      )

(require 'org)
(require 'ox)

(setq org-latex-pdf-process
'("pdflatex -interaction nonstopmode -output-directory %o %f" "pdflatex -interaction nonstopmode -output-directory %o %f" "pdflatex -interaction nonstopmode -output-directory %o %f"))

;  '("latexmk -pdflatex='pdflatex -interaction nonstopmode' -pdf -bibtex -f %f"))

;stuff to facilitate editing xqueries for berryPIM
(defun xquery-oneline-to-multi()
  (interactive)
  (save-excursion (while (re-search-forward "for" nil t)
                    (replace-match "
for" nil nil)))
  (save-excursion (while (re-search-forward "let" nil t)
                    (replace-match "
let" nil nil)))
  (save-excursion (while (re-search-forward "order by" nil t)
                    (replace-match "
order by" nil nil)))
  (save-excursion (while (re-search-forward "where" nil t)
                    (replace-match "
where" nil nil)))
  (save-excursion (while (re-search-forward "return" nil t)
                    (replace-match "
return" nil nil)))
  )

(defun xquery-multiline-to-one()
  (interactive)
  (save-excursion (while (re-search-forward "
for" nil t)
                    (replace-match "for" nil nil)))
  (save-excursion (while (re-search-forward "
let" nil t)
                    (replace-match "let" nil nil)))
  (save-excursion (while (re-search-forward "
order by" nil t)
                    (replace-match "order by" nil nil)))
  (save-excursion (while (re-search-forward "
where" nil t)
                    (replace-match "where" nil nil)))
  (save-excursion (while (re-search-forward "
return" nil t)
                    (replace-match "return" nil nil))))

(defun open-favorites ()
  (interactive)
;  (load "/projects/berrywebsite/org_structured_exporter/exporter.el")
  (find-file "~/.emacs")
  (find-file "~/Desktop/berryFiles/currell/essaysdoc.org")
  (find-file "~/helpful.el")
  (find-file "~/helpful.sh")
  (find-file "~/helpful.lisp")
)

(setq org-agenda-files '("~/Desktop/berryFiles/currell/essaysdoc.org"))

(setq inferior-lisp-program "/usr/local/bin/sbcl")

(defun b-nextbar ()
  (interactive)
  (next-line)
  (while (not (equal (char-after (point)) ?─))
    (next-line)))

(defun b-prevbar ()
  (interactive)
  (previous-line)
  (while (not (equal (char-after (point)) ?─))
    (previous-line)))


;; a small performance tester
(defun doprimecheck ()
  (interactive)
  (setf *start* (current-time))
  (setf *results* '())
  (cl-loop for i from 3 to 100000 do
           (let ((canbeprime t))
             (cl-loop for i2 from 2 to (sqrt i) do
                      (if (= 0 (mod i i2))
                          (progn
                            (setf canbeprime nil)
                            (cl-return))))
             (if canbeprime (setf *results* (cons i *results*)))))
  (setf *results* (reverse *results*))
  (setf *timeelapsed* (subtract-time *start* (current-time)))
  (print (format "time elapsed: %s" *timeelapsed*))
  (print *results*)
  *results*
  )


(defun unpop-to-mark-command ()
  "Unpop off mark ring. Does nothing if mark ring is empty."
  (interactive)
      (when mark-ring
        (setq mark-ring (cons (copy-marker (mark-marker)) mark-ring))
        (set-marker (mark-marker) (car (last mark-ring)) (current-buffer))
        (when (null (mark t)) (ding))
        (setq mark-ring (nbutlast mark-ring))
        (goto-char (marker-position (car (last mark-ring))))))

;(global-set-key (kbd "M-.") 'ggtags-find-tag-dwim)
;(define-key evil-normal-state-map (kbd "M-.") 'ggtags-find-tag-dwim)
;(define-key evil-insert-state-map (kbd "M-.") 'ggtags-find-tag-dwim)
(global-set-key (kbd "M-.") 'slime-edit-definition)
(define-key evil-normal-state-map (kbd "M-.") 'slime-edit-definition)
(define-key evil-insert-state-map (kbd "M-.") 'slime-edit-definition)


(defun append-global-c-tags-to-tagslist ()
  (interactive)
  (setf  tags-table-list (append (list "/home/vancan1ty/C/TAGS") tags-table-list))
  (setf tags-file-name nil)
  )

(global-set-key (kbd "C-c C-g") 'grep)

(setq load-path (cons "/usr/share/emacs/site-lisp/global" load-path))
(autoload 'gtags-mode "gtags" "" t)

(defmacro print-transparent (exp)
  `(let ((tvar987 ,exp)) (message (format "at: %s, '%s'\n" ,(line-number-at-pos) tvar987)) tvar987))

(defmacro print-transparent-2 (note exp)
  `(let ((tvar987 ,exp)) (message (format "%s.  at: %s, '%s'\n" ,note ,(line-number-at-pos) tvar987)) tvar987))

(setf completion-styles '(substring partial-completion basic emacs22))

(setf backward-forward-evil-compatibility-mode t)
(require 'backward-forward)
(backward-forward-mode t)

(load "/projects/berrywebsite/org_structured_exporter/exporter.el")
(setf *genroot* "/projects/berrywebsite/website_2015_content/generated_content")

(setq ido-default-buffer-method 'maybe-frame)

(defun slurp (f)
  (with-temp-buffer
    (insert-file-contents f)
    (buffer-substring-no-properties
       (point-min)
       (point-max))))

(defun slurp-lisp (filename)
  (with-temp-buffer
    (insert-file-contents filename)
    (read
     (buffer-substring-no-properties
      (point-min)
      (point-max)))))

(defun spit-lisp (filename lispobject)
  (with-temp-file filename
    (print lispobject (current-buffer))))

(defun gtd-new-action ()
  (interactive)
  (let* ((actions (slurp-lisp "~/edata/actionslist.txt"))
         (lastaction (elt actions 0))
         (newactionnumber (if (> (length actions) 0) (1+ (elt lastaction 0)) 1))
         (newactiondatestr (format-time-string "%Y%m%d"))
         (newaction (list newactionnumber newactiondatestr))
         (newactionslist (cons newaction actions))
         (shortform (format ":%s:A%d:" newactiondatestr newactionnumber)))
    (insert shortform)
    (spit-lisp "~/edata/actionslist.txt" newactionslist)))

(defun gtd-date()
  (interactive)
  (insert  (format-time-string "%Y%m%d")))

(defun gtd-jump(section-name)
  (interactive
   (list (ido-completing-read "choose section: " '("TASKLISTS" "Daily" "Personal" "TimeTracking" "Structured"))))
  (goto-char (point-min))
  (let ((searchstr (concat "\\*+ " section-name)))
    ;(message searchstr)
    (re-search-forward searchstr)
    (beginning-of-line)))

;;(global-set-key (kbd "C-c j") 'gtd-jump)
;;(global-set-key (kbd "C-c n") 'gtd-new-action)
;;(global-set-key (kbd "C-c d") 'gtd-date)
(define-key org-mode-map (kbd "C-c j") 'gtd-jump)
(define-key org-mode-map (kbd "C-c n") 'gtd-new-action)
(define-key org-mode-map (kbd "C-c d") 'gtd-date)
(define-key org-mode-map (kbd "C-c b") (lambda () (interactive) (insert "#+BEGIN_SRC
")))
(define-key org-mode-map (kbd "C-c e") (lambda () (interactive) (insert "#+END_SRC
")))

;;; below we have my configuration for opening new buffers
;;; based on https://www.simplify.ba/articles/2016/01/25/display-buffer-alist/
(window-list-1 nil nil t)


(defun berry-lispenv-display-buffer (buffer &optional alist)
  "Select window for BUFFER (need to use word ALIST on the first line).
Returns thirth visible window if there are three visible windows, nil otherwise.
Minibuffer is ignored."
  (cl-block berry-lispenv-display-buffer
    (let* ((winlist (window-list-1 nil nil t))
           (outindex 0))
      (while (< outindex (length winlist))
        (let ((candidate (elt winlist outindex)))
          (if (eql t (window-parameter candidate 'berrydesignated))
              (progn
                ;;            (message "helloooo!")
                (set-window-buffer candidate buffer)
                (cl-return-from berry-lispenv-display-buffer candidate)))
          (incf outindex)
          ))
      nil)))

;; CB do the below manually, then run setup-lispenv
;(set-window-parameter (elt (window-list-1 nil nil t) 2) 'berrydesignated t)
(defun setup-lispenv ()
  "this sets up a lisp programming environment for the following two monitor layout:
                |-------------|
|-repl---|      |      |      |
|--------|      | code | code |
|-rand---|      |------|------|
"
  (interactive)
  (add-to-list 'display-buffer-alist
               `(".*" .
                 ((display-buffer-reuse-window
                   berry-lispenv-display-buffer
                   display-buffer-in-side-window
                   )
                  .
                  ((reusable-frames     . visible)
                   (side                . bottom)
                   (window-height       . 0.50)))
                 )))
(setf display-buffer-alist ())

;(window-parameter (elt (window-list-1 nil nil t) 1) 'berrydesignated)
(window-parameters (elt (window-list-1 nil nil t) 1))
;(set-window-parameter (elt (window-list-1 nil nil t) 1) 'berrydesignated t)

(setq solarized-scale-org-headlines nil)

;; (custom-set-faces
;; ;; custom-set-faces was added by Custom.
;; ;; If you edit it by hand, you could mess it up, so be careful.
;; ;; Your init file should contain only one such instance.
;; ;; If there is more than one, they won't work right.
;; '(rainbow-delimiters-depth-1-face ((t (:foreground "dark orange"))))
;; '(rainbow-delimiters-depth-2-face ((t (:foreground "deep pink"))))
;; '(rainbow-delimiters-depth-3-face ((t (:foreground "chartreuse"))))
;; '(rainbow-delimiters-depth-4-face ((t (:foreground "deep sky blue"))))
;; '(rainbow-delimiters-depth-5-face ((t (:foreground "yellow"))))
;; '(rainbow-delimiters-depth-6-face ((t (:foreground "orchid"))))
;; '(rainbow-delimiters-depth-7-face ((t (:foreground "spring green"))))
;; '(rainbow-delimiters-depth-8-face ((t (:foreground "sienna1")))))

(require 'slime-autoloads)
(setq slime-contribs '(slime-fancy))
